#!/bin/bash

# Log file and secure password storage
LOG_FILE="/var/log/user_management.log"
SECURE_DIR="/var/secure"
PASSWORD_FILE="$SECURE_DIR/user_passwords.csv"

# Ensure secure directory exists and set proper permissions
mkdir -p $SECURE_DIR
chmod 700 $SECURE_DIR
touch $PASSWORD_FILE
chmod 600 $PASSWORD_FILE

# Function to create user and set up groups
create_user() {
    local username=$1
    local groups=$2

    # Check if user already exists
    if id "$username" &>/dev/null; then
        echo "User $username already exists" | tee -a $LOG_FILE
    else
        # Create user with home directory and personal group
        useradd -m -s /bin/bash -g $username $username
        echo "User $username created" | tee -a $LOG_FILE

        # Create specified groups if they don't exist and add user to them
        IFS=',' read -r -a group_array <<< "$groups"
        for group in "${group_array[@]}"; do
            if ! getent group "$group" > /dev/null 2>&1; then
                groupadd "$group"
                echo "Group $group created" | tee -a $LOG_FILE
            fi
            usermod -aG "$group" "$username"
        done
        echo "User $username added to groups $groups" | tee -a $LOG_FILE

        # Set permissions for home directory
        chmod 700 /home/$username
        chown $username:$username /home/$username
        echo "Home directory for $username set up with appropriate permissions" | tee -a $LOG_FILE

        # Generate random password
        password=$(openssl rand -base64 12)
        echo $username,$password >> $PASSWORD_FILE
        echo "Password for $username generated and stored securely" | tee -a $LOG_FILE
    fi
}

# Ensure the text file is provided as an argument
if [ $# -ne 1 ]; then
    echo "Usage: $0 <name-of-text-file>"
    exit 1
fi

# Read the text file and process each line
while IFS=';' read -r username groups; do
    # Remove whitespace
    username=$(echo "$username" | xargs)
    groups=$(echo "$groups" | xargs)

    # Check if the personal group already exists before creating it
    if ! getent group "$username" > /dev/null 2>&1; then
        groupadd $username
        echo "Group $username created" | tee -a $LOG_FILE
    fi

    # Create user and assign groups
    create_user $username "$groups"
done < $1

echo "All users created and actions logged to $LOG_FILE"
