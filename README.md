# User and Group Management Script

This repository contains a bash script `create_users.sh` that automates the process of creating users and groups, setting up home directories, generating random passwords, and logging all actions.

## Prerequisites

- Ubuntu or any other Linux distribution
- Bash shell
- OpenSSL

## Usage

1. **Clone the repository**:
    ```bash
    git clone https://gitlab.com/theadeyemiolayinka-hng-11/devops/user-and-group-management-script
    cd user-and-group-management-script
    ```

2. **Create a text file with usernames and groups**:
    - Each line should be in the format `username;groups`
    - Example:
        ```
        light; sudo,dev,www-data
        idimma; sudo
        mayowa; dev,www-data
        ```

3. **Run the script with sudo**:
    ```bash
    sudo bash create_users.sh <name-of-text-file>
    ```

## Files

- `create_users.sh`: The main script file
- `README.md`: This readme file

## Logging and Password Storage

- Actions are logged to `/var/log/user_management.log`.
- Generated passwords are stored securely in `/var/secure/user_passwords.csv` with restricted permissions.

## Author
- [TheAdeyemiOlayinka](https://theadeyemiolayinka.com)

## Blogpost
- [Automating User and Group Management with a Bash Script](https://theadeyemiolayinka.hashnode.dev/automating-user-and-group-management-with-a-bash-script)

## License

This project is licensed under the MIT License.